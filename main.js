var canvas = document.getElementById('canvas');
context = canvas.getContext('2d');
context.imageSmoothingEnabled = false;
context.webkitImageSmoothingEnabled = false;
context.mozImageSmoothingEnabled = false;
var toolindex=0;
var frame=0;
var tools=new Array()
tools[0]=new Array()
tools[0][0]=new Image();
tools[0][0].src="digger_up.png";
tools[0][1]=new Image()
tools[0][1].src="digger.png";

tools[1]=new Array()
tools[1][0]=new Image();
tools[1][0].src="excavator.png";
tools[1][1]=new Image();
tools[1][1].src="excavator_down.png";


var dust= new Image();
dust.src="dust.png";
var dirttile=new Image();
dirttile.src="dirt.png";


var pos=0;
var ypos=5*(context.canvas.height/3);
var step=0;
var miner_stage=0;
var rectanglepos=80;

var pixels=parseInt(getCookie("amount")) || 0;

var speed=18;
function update() {
    context.save();
    step+=3;
    context.canvas.width=window.innerWidth;
    context.canvas.height=window.innerHeight;
    pos+=3;
    if(rectanglepos>=0) {
        rectanglepos-=3;
    }
    if(step>=10) {
        frame++;
        if(frame>=tools[toolindex].length) {
            frame=0;
        }
        step=0;
    }
    context.fillStyle="#91bbff";
    context.fillRect(0,0,context.canvas.width,context.canvas.height);
    var dirtpattern=context.createPattern(dirttile,"repeat");
    context.translate(0,rectanglepos);
    context.fillStyle=dirtpattern;
    context.fillRect(0,ypos,context.canvas.width,context.canvas.height);
    context.fillStyle="#91bbff";
    context.fillRect(0,0,pos+16,ypos+32);
    context.stroke();
    context.arc(pos+16,ypos,32,1.5*Math.PI,(0.5*Math.PI)+0.5);
    context.fillStyle="#91bbff";
    context.fill();
    context.drawImage(tools[toolindex][frame],pos,ypos,tools[toolindex][frame].width,tools[toolindex][frame].height);
    for(var i=0; i<5; i++) {
        context.drawImage(dust,pos+getRandomInt(3,20),ypos+getRandomInt(3,10));
    }
    dustone=!dustone;
    context.fillStyle="black";
    context.font="30px Arial";
    context.fillText(pixels + " pixels",10,30-rectanglepos);
    context.restore();
    if(pos>=context.canvas.width+40){
        pos=-200;
        rectanglepos=32;
        window.requestAnimationFrame(update);
    }
    pixels+=1;
}
var gameloop=setInterval(function(){
    window.requestAnimationFrame(update);
    
},speed);
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
document.body.onkeydown = function(e){
    if(e.keyCode == 32){
        speed=1;
        clearInterval(gameloop);
        gameloop=setInterval(function(){
            window.requestAnimationFrame(update);
        },speed);
    }
}
document.body.onkeyup = function(e) {
    if(e.keyCode == 32){
        speed=18;
        clearInterval(gameloop);
        gameloop=setInterval(function(){
            window.requestAnimationFrame(update);
        },speed);
    }
};

window.onbeforeunload = function(e) {
    document.cookie = "amount="+pixels;
};




function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }